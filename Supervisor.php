<?php 
    
    abstract class Supervisor {     
        protected $slogan;    
         
        abstract public function saySloganOutLoud();   
    }
    // trait
    trait Active {
        public function defindYourSelf() {
            echo  get_class($this);
        }
    }
    // interface
    interface Boss {
        public function checkValidSlogan();
    }
    // class EasyBoss
    class EasyBoss extends Supervisor implements Boss {
        use Active;

        function setSlogan($slogan) {
            $this->slogan = $slogan;
        }
     
        public function checkValidSlogan() {
            if (substr_count($this->slogan,'before')!=0 || substr_count($this->slogan,'after')!=0) {
                return true;
            } else {
                return false;
            }
        }

        public function saySloganOutLoud() {
            echo "Start";
        }
    }
    // class UglyBoss
    class UglyBoss extends Supervisor implements Boss {
        use Active;

        function setSlogan($slogan) {
            $this->slogan = $slogan;
        }

        public function checkValidSlogan() {
            if (substr_count($this->slogan,'before')!=0 && substr_count($this->slogan,'after')!=0) {
                return true;
            } else {
                return false;
            }
        }    
        
        public function saySloganOutLoud() {
            echo "Start";
        }
    }

    // Khoi tao doi tuong
    $easyBoss = new EasyBoss();
    $uglyBoss = new UglyBoss();
    // thiet lap chuoi vao
    $easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');
    $uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');
    
    $easyBoss->saySloganOutLoud(); 
    echo "<br>";
    $uglyBoss->saySloganOutLoud(); 
    echo "<br>";
    // ket qua kiem tra chuoi
    var_dump($easyBoss->checkValidSlogan()); // true
    echo "<br>";
    var_dump($uglyBoss->checkValidSlogan()); // true
    echo "<br>"; 
    // In ten class
    echo 'I am '.$easyBoss->defindYourSelf();
    echo "<br>";        
    echo 'I am '.$uglyBoss->defindYourSelf(); 
?>