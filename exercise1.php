<?php

function checkValidString($str){
    if (strlen($str)==0) {
        return true;
    } elseif (strlen($str)>50 && substr_count($str,"after")==0) {
        return true;               
    } elseif (substr_count($str,"before")!=0) {
        return true;      
    } else {
        return false;       
    }
}

$data = @fopen('file1.txt',"r");
$len = filesize('file1.txt');

if ($len==0){
    $str = '';   
} else {
    $str = fread($data, filesize('file1.txt'));
}
fclose($data);

if (checkValidString($str)) {
    echo "Chuoi hop le";
} else {
    echo "Chuoi khong hop le";
}

echo "<br>";

$data = @fopen('file2.txt',"r");
$len = filesize('file2.txt');

if ($len==0){
    $str = '';
} else {
    $str = fread($data, filesize('file2.txt')); 
}
fclose($data);

if (checkValidString($str)) {
    echo "Chuoi hop le";
} else {
    echo "Chuoi khong hop le";
}
?>