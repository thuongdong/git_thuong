<?php
    /**
     * check valid email
     *
     * @param email
     * @return boolean
     */
    function valid_email($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * check length of email
     *
     * @param email
     * @return boolean
     */
    function length_email($email)
    {
        return strlen(($email) < 256);
    }

    /**
     * check length of password
     *
     * @param password
     * @return boolean
     */
    function length_password($password)
    {
        return strlen($password) >= 6 && strlen($password) <= 50;
    } 

    $errors = array();
    $data = array();

    // if click button Login
    if (isset($_POST['signupform'])) {
        //get data
        $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
        $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
        $data['repassword'] = isset($_POST['repassword']) ? $_POST['repassword'] : '';

        // validate email
        if (empty($data['email'])) {
            $errors['email'] = 'Bạn chưa nhập email';
        } elseif (!valid_email($data['email']) && !length_email($data['email'])) {
            $errors['email'] = 'Email không đúng định dạng và độ dài vượt quá 255 ký tự'; 
        } elseif (!valid_email($data['email']) && length_email($data['email'])) {
            $errors['email'] = 'Email không đúng định dạng';
        } elseif (valid_email($data['email']) && !length_email($data['email'])) {
            $errors['email'] = 'Email vượt quá 256 ký tự';
        }

        // validate password
        if (empty($data['password'])) {
            $errors['password'] = 'Bạn chưa nhập password';
        } elseif (!length_password($data['password'])) {
            $errors['password'] = 'Password chỉ cho phép độ dài từ 6-50 ký tự';   
        }  
        
        // validate re-password
        if (empty($data['repassword'])) {
            $errors['repassword'] = 'Bạn chưa nhập re-password';
        } elseif ($data['repassword'] !== $data['password']) {
            $errors['repassword'] = 'Password không trùng khớp';   
        }

        // work with database
        try {
            $conn = new PDO(
                'mysql:host=localhost; dbname=thuong; charset=utf8',
                'root',
                ''
            );
            if (!$errors) {
                // prepare sql
                $stmt = $conn->prepare("INSERT INTO users (mail_address, password)
                VALUES (:email, :password)");

                // bind parameters
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':password', $password);

                $email = $data['email'];
                $password = $data['password'];

                $stmt->execute();
            }
        } catch (PDOException $ex) {
            echo 'Kết nối đến database không thành công';
        }

        if (isset($_POST['signupform'])) 
        {
            if (!$errors) {
                echo '<div class="alert alert-success">Đăng ký thành công</div>';
                header('Location: LoginPdo.php');
            } else {
                echo '<div class="alert alert-danger">Đăng ký thất bại <br>';
            }
        }
    }
?>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trang đăng ký</title>
    <link rel="stylesheet" href="bootstrap-4.0.0-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-4.0.0-dist/js/bootstrap.min.js">
    <style type="text/css">
        .example{
            margin: 20px;
        }
 
    </style>
</head>
<body>
    <div class="example">
        <div class="container">
            <div class="row">
                <form class="form-horizontal" method="POST" action="RegisterPdo.php">
                    <div class="form-group">
                        <label class="control-label col-xs-2">Đăng ký</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-2">Email</label>
                        <div class="col-xs-10">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                            <?php echo isset($errors['email']) ? '<div class="alert alert-danger">' . $errors['email'] . '</div>' : ''; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-2">Mật khẩu</label>
                        <div class="col-xs-10">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <?php echo isset($errors['password']) ? '<div class="alert alert-danger">' . $errors['password'] . '</div>' : ''; ?>
                        </div>   
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-2">Nhập lại mật khẩu</label>
                        <div class="col-xs-10">
                            <input type="password" class="form-control" name="repassword" placeholder="Re-password">
                            <?php echo isset($errors['repassword']) ? '<div class="alert alert-danger">' . $errors['repassword'] . '</div>' : ''; ?>
                        </div>   
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-2 col-xs-10">    
                            <button type="submit" name="signupform" class="btn btn-primary">Đăng ký</button>
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
</body>
</html>
