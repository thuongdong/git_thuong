<?php

    class HandleString {

        // khai bao thuoc tinh
        private $check1;
        private $check2;

        // ham setheck
        public function setCheck($check1, $check2)
        {
            $this->check1 = $check1;
            $this->check2 = $check2;
        }
            
        // phuong thuc readFile() de doc cac file.txt
        public function readFile($file) {
            $data = fopen($file, "r");
            if (!$data) {
                return false;
            } else {
                $str = fread($data, filesize($file));
                if ($str) return $str;
            }
        }
       
        // function kiem tra chuoi
        public function checkValdString($str) {
            if (strlen($str)==0) {
                return true;
            } elseif (strlen($str)>50 && substr_count($str,"after")==0) {
                return true;               
            } elseif (substr_count($str,"before")!=0) {
                return true;      
            } else {
                return false;       
            }
        }
    }

    // Khoi tao doi tuong object1
    $object1 = new HandleString();
    $check1 = $object1->checkValdString($object1->readFile("file1.txt"));
    $check2 = $object1->checkValdString($object1->readFile("file2.txt"));
    $object1->setCheck($check1, $check2);
?>
