<?php
    session_start();
    
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // if click button Log out
        if (isset($_POST['logout_action'])) {
            // delete session
            session_destroy();
        }
    }
?>
<html>
<head>
    <title>Login Success</title>
</head>
<body>
    <form method="POST" action="LoginSuccess.php">
        <a href="LoginPdo.php"><input type="button" name="logout_action" value="Đăng xuất"></a>
        <p>Ấn đăng xuất sẽ quay trở về trang đăng nhập</p>
    </form>
</body>
</html>
