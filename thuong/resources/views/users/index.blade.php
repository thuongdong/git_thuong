@extends('layouts.default')
@section('title', 'Danh sách người dùng')

@section('header')
    <div class="row">
    @parent
        <p> Đây là trang danh sách người dùng </p> 
    </div>
@endsection

@section('content')
    <div class="row">
        <h2>Tìm kiếm người dùng</h2>
        <form method="get" action="{{ route('users.index') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label>Địa chỉ email</label>
                <input type="mail_address" class="form-control" name="mail_address" placeholder="Nhập địa chỉ email">
            </div>
            <div class="form-group">
                <label>Tên</label>
                <input type="name" class="form-control" name="name" placeholder="Nhập tên">
            </div>
            <div class="form-group">
                <label>Địa chỉ</label>
                <input type="address" class="form-control" name="address" placeholder="Nhập địa chỉ">
            </div>
            <div class="form-group">
                <label>Số điện thoại</label>
                <input type="phone" class="form-control" name="phone" placeholder="Nhập số điện thoại">
            </div>
            <div class="form-group">
                <input class="btn btn-info" type="submit" name="submit" value="Tìm kiếm">
            </div>
        </form>
    </div>
    <div class="row">
        <h2>Danh sách người sử dụng</h2>  
        <a href=" {{ route('users.create') }}" class="btn btn-info"> Đăng kí </a>   
        @include('flash::message')
            <div class="table-responsive">   
            <table class="table">
                <thead>
                    <tr>
                    <th class="text-center">Số thứ tự</th>
                    <th class="text-center">Địa chỉ email</th>
                    <th class="text-center">Tên</th>
                    <th class="text-center">Địa chỉ</th>
                    <th class="text-center">Số điện thoại</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                    <td class="text-left">{{ (($users->currentPage() - 1 ) * $users->perPage() ) + $loop->iteration }}</td>
                    <td class="text-left">{{ $user->mail_address }}</td>
                    <td class="text-left">{{ Helper::toUpperCase($user->name) }}</td>
                    <td class="text-left">{{ $user->address }}</td>
                    <td class="text-left">{{ $user->phone }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $users->links() }}
        </div>
    </div>
@endsection
        