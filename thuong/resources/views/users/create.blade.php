@extends('layouts.default')

@section('title', 'Thêm mới người dùng')

@section('header')
    <div class="row">
        @parent
        <p> Đây là trang đăng ký người dùng </p> 
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <form action="{{ route('users.store')}}" method="post" id="fileForm" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <fieldset><legend class="text-center">Nhập thông tin để đăng ký bắt buộc *</small></span></legend>
        <div class="form-group @if($errors->has('mail_address')) alert alert-danger @endif">
            <label for="email"><span class="req">* </span> Địa chỉ email: </label> 
                <input class="form-control" type="text" name="mail_address" id = "email" />  
            @if($errors->has('mail_address'))
                {{ $errors->first('mail_address') }}
            @endif
        </div>

        <div class="form-group {{ $errors->has('password') ? 'alert alert-danger' : '' }}">
            <label for="password"><span class="req">* </span> Mật khẩu: </label>
                <input name="password" type="password" class="form-control" id="pass1" /> </p>
            @if($errors->has('password'))
                {{ $errors->first('password') }}
            @endif
        </div>

        <div class="form-group @if($errors->has('password_confirmation')) {{'alert alert-danger'}} @endif">
            <label for="password"><span class="req">* </span> Mật khẩu xác nhận: </label>
                <input name="password_confirmation" type="password" class="form-control"/>
                    <span id="confirmMessage" class="confirmMessage"></span>
                @if($errors->has('password_confirmation'))
                    {{ $errors->first('password_confirmation') }}
                @endif
        </div>

        <div class="form-group @if($errors->has('name')) {{'alert alert-danger'}} @endif">     
            <label for="name"><span class="req">* </span> Tên: </label>
                <input class="form-control" type="text" name="name" id = "txt"/> 
            @if($errors->has('name'))
                {{ $errors->first('name') }}
            @endif
        </div>

        <div class="form-group @if($errors->has('address')) {{'alert alert-danger'}} @endif">
            <label for="address"><span class="req">* </span> Địa chỉ: </label> 
                <input class="form-control" type="text" name="address" id = "txt"  placeholder="" />
            @if($errors->has('address'))
                {{ $errors->first('address') }}
            @endif  
        </div>

        <div class="form-group @if($errors->has('phone')) {{'alert alert-danger'}} @endif">     
            <label for="phone"><span class="req">* </span> Số điện thoại: </label>
                <input class="form-control" type="text" name="phone" id = "txt" /> 
            @if($errors->has('phone'))
                {{ $errors->first('phone') }}
            @endif
        </div>

        <div class="form-group">
            <input class="btn btn-success" type="submit" name="submit_reg" value="Đăng ký">
        </div>
        </fieldset>     
        </form><!-- ends register form -->
    </div><!-- ends col-6 -->            
</div>
@endsection
