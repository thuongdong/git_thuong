<html>
    @include('layouts.head')
    <body>
        <div class="container">
            @include('layouts.header')
            @yield('content')
        </div>

        @include('layouts.footer')
    </body>
</html>
