<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use Helper;

class UserController extends Controller
{
    /**
     * The user implementation.
     *
     * @var User
     */
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @param User $users
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Show all users, return view a list of users
     *
     * @param 
     * @return view users.index
     */
    public function index(Request $request) 
    {
        $data = $request->all();
        $users = $this->user->getUser($data);
        return view('users.index', ['users' => $users]);
    }

    /**
     * Redirect to view create an user
     *
     * @param 
     * @return view users.create
     */
    public function create() 
    {
    	return view('users.create');
    }

    /**
     * Store user from request and 
     *
     * @param Request $request
     * @return view users.index
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->all();
        $this->user->createUser($data);
        flash(__('message.add_user_successfully'))->success();
        return redirect()->route('users.index');
    }
}
