<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Hash;

class User extends Authenticatable
{
    use SoftDeletes;    
    use Notifiable;

    protected $table = 'users';
    
    /**
     * The attributes that are mupdates the time.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mail_address',
        'password',
        'name',
        'address',
        'phone',

    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Create new user in table Users 
     *
     * @param array $data
     * @return 
     */
    public function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        return User::create($data);

    }

    /**
     * 
     */
    public function getUser(array $data)
    {
        $builder = User::orderBy('mail_address', 'ASC');

        if (isset($data['mail_address']) && $data['mail_address'] != '') {
            $builder->where('mail_address', 'LIKE', '%' . $data['mail_address'] . '%');
        }

        if (isset($data['name']) && $data['name'] != '') {
            $builder->where('name', 'LIKE', '%' . $data['name'] . '%');
        }

        if (isset($data['address']) && $data['address'] != '') {
            $builder->where('address', 'LIKE', '%' . $data['address'] . '%');
        }

        if (isset($data['phone']) && $data['phone'] != '') {
            $builder->where('phone', $data['phone']);
        }

        return $builder->paginate(20);

    }
}


