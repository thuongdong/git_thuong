<?php

namespace App\Facades;

class Helper
{
    public function toUpperCase($str)
    {
        return mb_strtoupper($str, 'UTF-8');
    }
}
