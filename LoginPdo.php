<?php
    // start session
    session_start();
?>
<?php
    /**
     * check valid email
     *
     * @param email
     * @return boolean
     */
    function valid_email($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * check length of email
     *
     * @param email
     * @return boolean
     */
    function length_email($email)
    {
        return strlen(($email) < 256);
    } 

    /**
     * check length of password
     *
     * @param password
     * @return boolean
     */
    function length_password($password)
    {
        return strlen($password) >= 6 && strlen($password) <= 50;
    } 

    $errors = array();
    $data = array();

    // if click button Log in
    if (isset($_POST['loginform'])) {
        // get data from method post
        $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
        $data['password'] = isset($_POST['password']) ? $_POST['password'] : '';
        $data['remember_me'] = isset($_POST['remember_me']) ? $_POST['remember_me'] : '';

        // validate email
        if (empty($data['email'])) {
            $errors['email'] = 'Bạn chưa nhập email';
        } elseif (!valid_email($data['email']) && !length_email($data['email'])) {
            $errors['email'] = 'Email không đúng định dạng và độ dài vượt quá 255 ký tự'; 
        } elseif (!valid_email($data['email']) && length_email($data['email'])) {
            $errors['email'] = 'Email không đúng định dạng';
        } elseif (valid_email($data['email']) && !length_email($data['email'])) {
            $errors['email'] = 'Email vượt quá 256 ký tự';
        }

        // validate password
        if (empty($data['password'])) {
            $errors['password'] = 'Bạn chưa nhập password';
        } elseif (!length_password($data['password'])) {
            $errors['password'] = 'Password chỉ cho phép độ dài từ 6-50 ký tự';   
        }

        // work with database
        try {
            $conn = new PDO(
                'mysql:host=localhost; dbname=thuong; charset=utf8',
                'root',
                ''
            );
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_CLASS);
            if (!$errors) {
                $stmt = $conn->prepare("SELECT mail_address,password FROM users WHERE mail_address=:email && password=:password");
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':password', $password);
                $email = $data['email'];
                $password = $data['password'];
                $stmt->setFetchMode(PDO::FETCH_CLASS);
                $stmt->execute();
                $result = $stmt->fetchAll();
                if (!$result) {
                    $errors['fail'] = 'Sai tên đăng nhập hoặc password';
                } else {
                    $_SESSION['email'] = $data['email'];
                    // set cookie
                    if($data['remember_me'] == 'on')
                    {
                        $hour = time() + 3600 * 24 * 30;
                        setcookie('email', $data['email'], $hour);
                        setcookie('password', $data['password'], $hour);
                    }
                }
            }
        } catch (PDOException $ex) {
            echo 'Kết nối đến database không thành công';
        }
    }

    if (isset($_POST["loginform"])) 
    {
        if (!$errors) {
            echo '<div class="alert alert-success">Đăng nhập thành công</div>';
            header('Location: LoginSuccessPdo.php');
        } else {
            echo '<div class="alert alert-danger">Đăng nhập thất bại <br>';
        }
    }
?>

<head>
    <meta charset="UTF-8">
    <title>Trang đăng nhập</title>
    <link rel="stylesheet" href="bootstrap-4.0.0-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-4.0.0-dist/js/bootstrap.min.js">
    <style type="text/css">
        .example{
            margin: 20px;
        }
    </style>
</head>
<body>
    <div class="example">
        <div class="container">
            <div class="row">
                <form class="form-horizontal" method="POST" action="LoginPdo.php">
                    <div class="form-group">
                        <label class="control-label col-xs-2">Đăng nhập</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-2">Email</label>
                        <div class="col-xs-10">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                            <?php echo isset($errors['email']) ? '<div class="alert alert-danger">' . $errors['email'] . '</div>' : ''; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-2">Mật khẩu</label>
                        <div class="col-xs-10">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <?php echo isset($errors['password']) ? '<div class="alert alert-danger">' . $errors['password'] . '</div>' : ''; ?>
                        </div>   
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-2 col-xs-10">    
                            <div class="checkbox">
                                <label><input type="checkbox" name="remember_me" value="">Ghi nhớ đăng nhập</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-offset-2 col-xs-10">    
                            <button type="submit" name="loginform" class="btn btn-primary">Đăng nhập</button> 
                            <button type="button" name="signinform" class="btn btn-primary"><a href="RegisterPdo.php">Đăng ký</a></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
