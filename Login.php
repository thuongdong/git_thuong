<?php
	session_start();
?>

<html>
	<head>
		<title>Trang dang nhap</title>
		<style>
			.error {color: #FF0000;}
		</style>
	</head>

	<body>
		<?php
			$Error = $emailErr =  $emailErr2 = $passErr = $loginFail =   "";
			// ket noi datase
			$conn = mysqli_connect("localhost","root","","Thuong") or die("không thể kết nối tới database");
			// Kiểm tra nếu người dùng đã ân nút đăng nhập thì mới xử lý
			if (isset($_POST["btn_submit"])) 
			{
				// lấy thông tin người dùng
				$email = $_POST["email"];
				$password = $_POST["password"];
				if (empty($_POST["email"]) || empty($_POST["password"])) {
					$Error =  "Email hoặc password bạn không được để trống!";
				} if (empty($_POST["email"]) || (!preg_match("/@/", $email)) || (strlen($_POST["email"]) < 6)) {
					$emailErr =  "Sai dinh dang email";
				} elseif (strlen($_POST["password"]) <6 || strlen($_POST["password"])>30) {  
					$passErr = "Mat khau tu 6-30 ki tu.";
				} else {
					$sql = "select * from users where email = '$email' and password = '$password' ";
					$query = mysqli_query($conn,$sql);
					$num_rows = mysqli_num_rows($query);
					if ($num_rows==0) {
						$loginFail = "Dang nhap that bai";
					} else {
						//tiến hành lưu tên đăng nhập vào session để tiện xử lý sau này
						$_SESSION['email'] = $email;
						if(!empty($_POST["remember"])) {
							setcookie ("member_login",$_POST["member_login"]);
							setcookie ("password",$_POST["password"]);
						} else {
							if(isset($_COOKIE["member_login"])) {
								setcookie ("member_login","");
							}
							if(isset($_COOKIE["password"])) {
								setcookie ("password","");
							}
						}
						// chuyen sang trang LoginSuccess.php
						header('Location: LoginSuccess.php');
					}
				}
			}
		?>

		<form method="POST" action="Login.php">
			<fieldset>
				<legend>Login</legend>
				<table>
					<tr>
						<td>Email</td>
						<td><input type="text" name="email" size="30" value="<?php if(isset($_COOKIE["member_login"])) { echo $_COOKIE["emamember_loginil"]; } ?>" ></td>
						<span class="error"> <?php echo $Error;?></span>						
						<span class="error"> <?php echo $emailErr;?></span>
						<span class="error"> <?php echo $emailErr2;?></span>						
					</tr>
					<tr>
						<td>Password</td>
						<td><input type="password" name="password" size="30" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>" ></td>
						<span class="error"> <?php echo $passErr;?></span>
					</tr>
					<tr>
						<td>Remember me</td>
						<td><input type="checkbox" name="remember" id="remember" <?php if(isset($_COOKIE["member_login"])) { ?> checked <?php } ?>  ></td>
					</tr>
					<tr>
						<td colspan="2" align="center"> <input name="btn_submit" type="submit" value="Login"></td>
						<span class="error"> <?php echo $loginFail;?></span>											
					</tr>
				</table>
			</fieldset>
		</form>
	</body>
</html>